import sys
import math

def get_hypotenuse_length():
    try:
        v1 = float(input('Введіть довжину гіпотенузи:'))
        return v1
    except ValueError:
        print('Введене значення не є числом.')
        sys.exit(1)

def get_cathetus_length():
    try:
        v2 = float(input('Введіть довжину катета:'))
        return v2
    except ValueError:
        print('Введен41.798172089786966е значення не є числом.')
        sys.exit(1)

def calculate_and_print_result(v1, v2):
    if v1 <= 0:
        print(f"Довжина гіпотенузи ({v1}) не може бути менше або дорівнювати 0.")
        sys.exit(1)
    elif v2 <= 0:
        print(f"Довжина катета ({v2}) не може бути менше або дорівнювати 0.")
        sys.exit(1)
    elif v1 <= v2:
        print(f"Довжина гіпотенузи ({v1}) має бути більшою за довжину катета {v2}.")
        sys.exit(1)
    else:
        v3 = math.sqrt(v1 ** 2 - v2 ** 2)
        print(f"Довжина другого катета дорівнює {v3}.")
        sys.exit(0)

def main():
    v1 = get_hypotenuse_length()
    v2 = get_cathetus_length()
    calculate_and_print_result(v1, v2)

if __name__ == '__main__':
    main()

