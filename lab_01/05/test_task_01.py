import unittest
import sys
from unittest.mock import patch
from task_01 import main
from io import StringIO

class MyTestCase(unittest.TestCase):    
    @patch('sys.stdin', StringIO('dammy'))
    @patch('sys.stdout',new_callable=StringIO)   
    def test_valid_input(self, stdout):
        with self.assertRaises(SystemExit) as cm:            
            main()
        self.assertEqual(cm.exception.code, 1)        
        self.assertEqual(stdout.getvalue(),"Введіть довжину гіпотенузи:Довжина гіпотенузи (-1.0) не може бути менше або дорівнювати 0.\n")
    @patch('sys.stdin', StringIO('5\n4\n'))
    @patch('sys.stdout', new_callable=StringIO)    
    def test_success(self, stdout):
        with self.assertRaises(SystemExit) as cm:           
            main()
        self.assertEqual(cm.exception.code, 0)       
        self.assertEqual(stdout.getvalue(), "Введіть довжину гіпотенузи:Введіть довжину катета:Довжина другого катета дорівнює 3.0.\n")

if __name__ == '__main__':
    unittest.main()
