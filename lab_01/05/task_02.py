import math
import sys

def calculate_z1(a, b, x, y):
    return (a - math.atan(x)) / (b + math.atan(y))

def calculate_z2(x, y):
    return math.cos(y + x)

def calculate_z3(z1, z2):
    return (z1 + z2) ** 2

def main():
    try:
        a = float(input("Введіть значення A: "))
        b = float(input("Введіть значення B: "))
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))

        denominator = b + math.atan(y)

        if denominator != 0:
            z1 = calculate_z1(a, b, x, y)
            z2 = calculate_z2(x, y)
            z3 = calculate_z3(z1, z2)

            print(f"Z1 = {z1}")
            print(f"Z2 = {z2}")
            print(f"Z3 = {z3}")

            sys.exit(0)
        else:
            print("Некоректні дані. Знаменник не може дорівнювати нулю.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для A, B, x та y.")
        sys.exit(1)

if __name__ == "__main__":
    main()
