import unittest
import sys
from unittest.mock import patch
from task_02 import main
from io import StringIO

class MyTestCase(unittest.TestCase):
    @patch('sys.stdin', StringIO('3\n0\n2\n0\n'))      
    @patch('sys.stdout',new_callable=StringIO)
    def test_valid_input(self, stdout):        
        with self.assertRaises(SystemExit) as cm:
            main()        
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(),"Введіть значення A: Некоректні дані. Введіть числові значення для A, B, x та y.\n")
    @patch('sys.stdin', StringIO('3\n5\n2\n1\n'))    
    @patch('sys.stdout', new_callable=StringIO)
    def test_success(self, stdout):        
        with self.assertRaises(SystemExit) as cm:
            main()        
        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(stdout.getvalue(), "Введіть значення A: Введіть значення B: Введіть значення x: Введіть значення y: Z1 = 0.8295926618612286\nZ2 = 0.004425697988050785\nZ3 = 0.6955866245656821\n")

if __name__ == '__main__':
    unittest.main()