import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1= ((x^2) / (2*x+x^1)) - ((3*x+y^2) / (2-y^2))
Z2 = Z1 / (4*x - y^2)
Z3 = |Z1 - Z2|
'''


def main():
    try:
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))
        if 2 * x + x ** 2 != 0:
            if 2 - y ** 2 != 0:
                z1 = (x ** 2) / 2 * x + x ** 2 - (3 * x + y ** 2) / 2 - y ** 2
                if isinstance(z1, float) and (4 * x - y ** 2) != 0:
                    z2 = z1 / (4 * x - y ** 2)
                    if isinstance(z1, float) and isinstance(z2, float):
                        z3 = abs(z1 - z2)
                        print(f"Z1 = {z1}")
                        print(f"Z2 = {z2}")
                        print(f"Z3 = {z3}")
                        sys.exit(0)
                    else:
                        print("Некоректні дані. Значення Z1 та Z2 повинні бути числами.")
                        sys.exit(1)
                else:
                    print("Некоректні дані. Значення Z1 не може бути рівним нулю або знаменник не може дорівнювати нулю.")
                    sys.exit(1)
        else:
            print("Некоректні дані. Знаменники не можуть дорівнювати нулю.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)


if __name__ == "__main__":
    main()
