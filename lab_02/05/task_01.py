import sys
import math

class Triangle:
    def __init__(self):
        self.hypotenuse_length = 0
        self.cathetus_length = 0

    def get_hypotenuse_length(self):
        try:
            self.hypotenuse_length = float(input('Введіть довжину гіпотенузи:'))
        except ValueError:
            print('Введене значення не є числом.')
            sys.exit(1)

    def get_cathetus_length(self):
        try:
            self.cathetus_length = float(input('Введіть довжину катета:'))
        except ValueError:
            print('Введене значення не є числом.')
            sys.exit(1)

    def calculate_and_print_result(self):
        if self.hypotenuse_length <= 0:
            print(f"Довжина гіпотенузи ({self.hypotenuse_length}) не може бути менше або дорівнювати 0.")
            sys.exit(1)
        elif self.cathetus_length <= 0:
            print(f"Довжина катета ({self.cathetus_length}) не може бути менше або дорівнювати 0.")
            sys.exit(1)
        elif self.hypotenuse_length <= self.cathetus_length:
            print(f"Довжина гіпотенузи ({self.hypotenuse_length}) має бути більшою за довжину катета {self.cathetus_length}.")
            sys.exit(1)
        else:
            v3 = math.sqrt(self.hypotenuse_length ** 2 - self.cathetus_length ** 2)
            print(f"Довжина другого катета дорівнює {v3}.")
            sys.exit(0)

def main():
    triangle = Triangle()
    triangle.get_hypotenuse_length()
    triangle.get_cathetus_length()
    triangle.calculate_and_print_result()

if __name__ == '__main__':
    main()