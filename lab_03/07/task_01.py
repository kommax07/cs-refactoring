import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (tan(x) / (1 + ctan(y))
Z2 = cos(y + x) - x / sin (y + x)
Z3 = exp^(Z1+Z2) * (Z1 / Z2)
'''


def main():
    try:
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))
        d = 1 + math.atan(y)
        if 1 + math.atan(y) != 0:
            z1 = math.tan(x) / d
            if math.sin(y + x) != 0:
                z2 = math.cos(y + x) - x / math.sin(y + x)
                if (math.cos(y + x) - x / math.sin(y + x)) != 0:
                    z3 = (math.exp(math.tan(x) / d) + (math.cos(y + x) - x)) * ((math.tan(x) / 1 + math.atan(y)) / math.cos(y + x) - x / math.sin(y + x))
                    print(f"Z1 = {z1}")
                    print(f"Z2 = {z2}")
                    print(f"Z3 = {z3}")
                    sys.exit(0)
                else:
                    print("Некоректні дані. Знаменник не може дорівнювати нулю.")
                    sys.exit(1)
            else:
                print("Некоректні дані. Знаменник не може дорівнювати нулю.")
                sys.exit(1)
        else:
            print("Некоректні дані. Знаменник не може дорівнювати нулю.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)


if __name__ == "__main__":
    main()
