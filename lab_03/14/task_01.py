import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (x - y)^3 / (5 + (sin(x))^2
Z2 = (x^2 + (cos(0.7*x))^-4)^1/2 / y^3
Z3 = |((tan(x))^2 + Z1) / Z2|
'''


def main():
    try:
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))
        d = 5 + (math.sin(x)) ** 2
        if 5 + (math.sin(x)) ** 2 != 0:
            z1 = ((x - y) ** 3) / d
            if (y ** 3) != 0:
                z2 = ((x ** 2) + (math.cos(0.7 * x)) ** (-4)) ** (1 / 2) / (y ** 3)
                if ((x ** 2) + (math.cos(0.7 * x)) ** (-4)) ** (1 / 2) / (y ** 3) != 0:
                    z3 = abs((math.tan(x) ** 2 + (((x - y) ** 3) / 5 + (math.sin(x)) ** 2)) / (((x ** 2) + (math.cos(0.7 * x)) ** (-4)) ** (1 / 2) / (y ** 3)))
                    print(f"Z1 = {z1}")
                    print(f"Z2 = {z2}")
                    print(f"Z3 = {z3}")
                    sys.exit(0)
                else:
                    print("Некоректні дані. Знаменник Z2 не може дорівнювати нулю.")
                    sys.exit(1)
            else:
                print("Некоректні дані. Знаменник не може дорівнювати нулю.")
                sys.exit(1)
        else:
            print("Некоректні дані. Знаменник не може дорівнювати нулю.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)


if __name__ == "__main__":
    main()
