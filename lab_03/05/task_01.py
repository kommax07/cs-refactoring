import math
import sys

def calculate_z1(a, b, x, y):
    denominator_z1 = b + math.atan(y)
    if denominator_z1 == 0:
        sys.exit("Некоректні дані. Знаменник Z1 не може дорівнювати нулю.")
    return (a - math.atan(x)) / denominator_z1

def calculate_z2(x, y):
    denominator_z2 = math.sin(y + x)
    if denominator_z2 == 0:
        sys.exit("Некоректні дані. Знаменник Z2 не може дорівнювати нулю.")
    return math.cos(y + x) / denominator_z2

def calculate_z3(z1, z2, a, b):
    denominator_z3 = a - b
    if denominator_z3 == 0:
        sys.exit("Некоректні дані. Знаменник Z3 не може дорівнювати нулю.")
    return (z1 + z2) ** 2 + 4 / denominator_z3

def main():
    try:
        a = float(input("Введіть значення A: "))
        b = float(input("Введіть значення B: "))
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))

        z1 = calculate_z1(a, b, x, y)
        z2 = calculate_z2(x, y)
        z3 = calculate_z3(z1, z2, a, b)

        print(f"Z1 = {z1}")
        print(f"Z2 = {z2}")
        print(f"Z3 = {z3}")

    except ValueError:
        sys.exit("Некоректні дані. Введіть числові значення для A, B, x та y.")

if __name__ == "__main__":
    main()

